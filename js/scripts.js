var compareApp = angular.module('compareApp', []);

compareApp.factory('productsService', function ($http) {
    return {
        ajaxGet: function (ajaxUrl) {
            return $http({
                url: ajaxUrl,
                method: 'GET'
            }).success(function (data) {
                return data;
            });
        }
    };
});

compareApp.controller('CompareCtrl', ['$scope', 'productsService', function($scope, productsService) {
	$scope.selectsOptions = [1,2,3,4,5];

	$scope.selectedProducts = {};
	$scope.rendered = [];

	$scope.products = [{
		"id": 1,
		"name": "Product 1",
		"size": "12x12x10 (mm)",
		"color": "Black"
	},
	{
		"id": 2,
		"name": "Product 2",
		"size": "12x12x10 (mm)",
		"color": "Black"
	},
	{
		"id": 3,
		"name": "Product 3",
		"size": "12x12x10 (mm)",
		"color": "Black"
	},
	{
		"id": 4,
		"name": "Product 4",
		"size": "12x12x10 (mm)",
		"color": "Black"
	},
	{
		"id": 5,
		"name": "Product 5",
		"size": "12x12x10 (mm)",
		"color": "Black"
	}];

	$scope.selectProduct = function (index, product) {
		$scope.selectedProducts[index] = product;
	};

	$scope.createSelects = function () {
		if ($scope.selectsNumber > $scope.rendered.length) {
			var selectsLength = $scope.selectsNumber - $scope.rendered.length;
			for (var i = 0;  i < selectsLength; i++) {
				var temp = {data: []};
				temp.data = $scope.products;
				$scope.rendered.push(temp);
			}
		} else {
			removeRendered($scope.selectsNumber);
			removeSelected();
		}
	};

	$scope.hasSelectedProducts = function () {
		return !isEmpty($scope.selectedProducts);
	};

	// Private functions
	/*
	var getProducts = (function() {
        productsService.ajaxGet(url).then(function(data){
            $scope.products = data;
        });           
    })();
	*/
	var removeRendered = function (selected) {
		var removeAmount = $scope.rendered.length - selected,
			index = selected - 1; // Get the position to cut the array with splice
		$scope.rendered.splice(selected, removeAmount);
	};
	var removeSelected = function () {
		for (var key in $scope.selectedProducts) {
			if (!$scope.rendered[key]) { // If current select doesn't exist anymore on rendered
				delete $scope.selectedProducts[key];
			}
		}
	};
	var isEmpty = function (obj) {
        // null and undefined are "empty"
        if (obj == null) {
            return true;
        }
        // Assume if it has a length property with a non-zero value
        // that that property is correct.
        if (obj.length > 0) {
            return false;
        }
        if (obj.length === 0) {
            return true;
        }
        // Otherwise, does it have any properties of its own?
        // Note that this doesn't handle
        // toString and valueOf enumeration bugs in IE < 9
        for (var key in obj) {
            if (Object.prototype.hasOwnProperty.call(obj, key)) {
                return false;
            }
        }
        return true;
    };
}]);

// Filter supposed to be used to exclude already selected products from the other select fields
compareApp.filter('unselectedProducts', function() {
	return function (currentList, index, selectedProducts) {
		var filteredList = [];
		for (var key in selectedProducts) {
			if (index !== key) {
				for (var i = 0;  i < currentList.length; i++) {
					if (currentList[i].id !== selectedProducts[key].id ) {
						filteredList.push(currentList[i]);
					}
				}
			}
		}
		return filteredList.length > 0 ? filteredList : currentList;
	}
});